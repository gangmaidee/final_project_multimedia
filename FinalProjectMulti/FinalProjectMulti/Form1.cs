﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Diagnostics;
using AForge;
using AForge.Video;
using AForge.Video.FFMPEG;
using AForge.Math;
using AForge.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Windows.Forms.DataVisualization.Charting;

namespace FinalProjectMulti
{
    public partial class Form1 : Form
    {        
        public Form1()
        {
            InitializeComponent();
            
        }
        
        public string path = "";

        private void openBtn_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = null;

            string formats = "All Files (*.*)|*.*| " +
                      " All Videos Files |*.dat; *.wmv; *.3g2; *.3gp; *.3gp2; *.3gpp; *.amv; *.asf;  *.avi; *.bin; *.cue; *.divx; *.dv; *.flv; *.gxf; *.iso; *.m1v; *.m2v; *.m2t; *.m2ts; *.m4v; " +
                      " *.mkv; *.mov; *.mp2; *.mp2v; *.mp4; *.mp4v; *.mpa; *.mpe; *.mpeg; *.mpeg1; *.mpeg2; *.mpeg4; *.mpg; *.mpv2; *.mts; *.nsv; *.nuv; *.ogg; *.ogm; *.ogv; *.ogx; *.ps; *.rec; *.rm; *.rmvb; *.tod; *.ts; *.tts; *.vob; *.vro; *.webm";

            openFileDialog1.Filter = formats;

            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string file = openFileDialog1.FileName;
                try
                {
                    pathText.Text = "Path : " + openFileDialog1.FileName;
                    path = openFileDialog1.FileName;

                    ThresholdBox.Enabled = true;
                    segNameBox.Enabled = true;
                    searchBtn.Enabled = true;
                    histogramBtn.Enabled = true;

                    VideoFileReader reader2 = new VideoFileReader();
                    reader2.Open(path);

                    int keyFrame = Convert.ToInt32(reader2.FrameCount);

                    if (keyFrame % 2 == 0)
                    { keyFrame = keyFrame / 2; }
                    else
                    { keyFrame = (keyFrame + 1) / 2; }

                    process p = new process();
                    p.progressBar1.Minimum = 0;
                    p.Show();

                    if (Convert.ToInt32(reader2.FrameCount) > 10000)
                    {
                        p.progressBar1.Maximum = 1;
                        for (int z = 0; z < 10; z++)
                        {
                            Bitmap videoFrame2 = reader2.ReadVideoFrame();
                            if (z == 1)
                            {
                                searchKfPb.Image = videoFrame2;
                                break;
                            }
                            videoFrame2.Dispose();
                            videoFrame2 = null;
                            p.progressBar1.Value = z;
                            int percent = (int)(((double)p.progressBar1.Value / (double)p.progressBar1.Maximum) * 100);
                            p.progressBar1.CreateGraphics().DrawString(percent.ToString() + "%", new Font("Arial", (float)8.25, FontStyle.Regular), Brushes.Black, new PointF(p.progressBar1.Width / 2 - 10, p.progressBar1.Height / 2 - 7));

                        }
                        p.Close();
                        reader2.Close();
                    }
                    else
                    {
                        p.progressBar1.Maximum = keyFrame - 1;
                        for (int z = 0; z < Convert.ToInt32(reader2.FrameCount); z++)
                        {
                            Bitmap videoFrame2 = reader2.ReadVideoFrame();
                            if (z == keyFrame)
                            {
                                searchKfPb.Image = videoFrame2;
                                break;
                            }
                            videoFrame2.Dispose();
                            videoFrame2 = null;
                            p.progressBar1.Value = z;
                            int percent = (int)(((double)p.progressBar1.Value / (double)p.progressBar1.Maximum) * 100);
                            p.progressBar1.CreateGraphics().DrawString(percent.ToString() + "%", new Font("Arial", (float)8.25, FontStyle.Regular), Brushes.Black, new PointF(p.progressBar1.Width / 2 - 10, p.progressBar1.Height / 2 - 7));
                        }
                        p.Close();
                        reader2.Close();
                    }
                    

                }
                catch (IOException)
                {
                    pathText.Text = "Path : Catch";
                }
            }

        }

        private void segmentBtn_Click(object sender, EventArgs e)
        {
            int distance;

            if (ThresholdBox.Text != "")
            {
                if (segNameBox.Text != "")
                {
                    if (int.TryParse(ThresholdBox.Text, out distance))
                    {
                        DialogResult result = folderBrowserDialog1.ShowDialog();
                        if (result == DialogResult.OK)
                        {
                            string save_path = folderBrowserDialog1.SelectedPath + @"\" + segNameBox.Text;
                            VideoSegmentation.VideoSegment(path, segNameBox.Text, save_path, Convert.ToInt32(ThresholdBox.Text));
                            ThresholdBox.Text = "";
                            segNameBox.Text = "";
                        }
                        //MessageBox.Show(segmentNameBox.Text.ToString());                
                    }
                    else
                    {
                        MessageBox.Show("Threshold must be a numeric value.");
                    }
                }
                else
                {
                    MessageBox.Show("Please type a name of output video.");
                }
            }
            else
            {
                MessageBox.Show("Please input a Threshold value.");
            }
            
        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            //textBox1.Text = "";
            //textBox1.Text = VideoSearch.VdoSearch(path);
            VdoSearch(path);
        }
        /*
        public struct vdo
        {
            private int _index;
            private int _euclDist;

            public int index
            {
                get { return _index; }
                set { _index = value; }
            }

            public int EuclDist
            {
                get { return _euclDist; }
                set { _euclDist = value; }
            }

            public vdo(int Index, int EuclDist)
            {
                this._index = Index;
                this._euclDist = EuclDist;
            }

        };
        */

        public struct vdo
        {
            private Double _index;
            private Double _euclDist;

            public Double index
            {
                get { return _index; }
                set { _index = value; }
            }

            public Double EuclDist
            {
                get { return _euclDist; }
                set { _euclDist = value; }
            }

            public vdo(Double Index, Double EuclDist)
            {
                this._index = Index;
                this._euclDist = EuclDist;
            }

        };

        public string VdoSearch(string path)
        {
            int[] redValues = new int[256], db_red = new int[256];
            int[] greenValues = new int[256], db_green = new int[256];
            int[] blueValues = new int[256], db_blue = new int[256];

            //int d_red = 0, d_green = 0, d_blue = 0;
            long d_red = 0, d_green = 0, d_blue = 0;
            Double EucDis = 0, KF_EucDis = 0;
            //int EucDis = 0, KF_EucDis = 0;
            List<vdo> myVdo = new List<vdo>();

            string name = "", vdo_shot = "", vdo_keyframe = "", vdo_red = "", vdo_green = "", vdo_blue = "";

            VideoFileReader reader = new VideoFileReader();
            reader.Open(path);

            int keyFrame = Convert.ToInt32(reader.FrameCount);

            if (keyFrame % 2 == 0)
            { keyFrame = keyFrame / 2; }
            else
            { keyFrame = (keyFrame + 1) / 2; }

            for (int z = 0; z < Convert.ToInt32(reader.FrameCount); z++)
            {
                Bitmap videoFrame = reader.ReadVideoFrame();
                if (z == keyFrame)
                {
                    ImageStatistics rgbStatistics = new ImageStatistics(videoFrame);
                    redValues = rgbStatistics.Red.Values;
                    greenValues = rgbStatistics.Green.Values;
                    blueValues = rgbStatistics.Blue.Values;
                    break;
                }
                videoFrame.Dispose();
                videoFrame = null;
            }

            reader.Close();

            string constr = ConfigurationManager.ConnectionStrings["Dbconnection"].ConnectionString;
            SqlConnection con = new SqlConnection(constr);
            con.Open();

            string strQuery = "SELECT * FROM video";

            SqlCommand sel_cmd = new SqlCommand(strQuery, con);
            SqlDataReader sql_read = sel_cmd.ExecuteReader();
            if (sql_read.HasRows)
            {
                while (sql_read.Read())
                {
                    d_red = 0; d_green = 0; d_blue = 0;
                    EucDis = 0;
                    //-----------------------------------------------------------
                    vdo_red = sql_read["vdo_red"].ToString();
                    vdo_green = sql_read["vdo_green"].ToString();
                    vdo_blue = sql_read["vdo_blue"].ToString();
                    string[] value_red = vdo_red.Split(','),
                        value_green = vdo_green.Split(','),
                        value_blue = vdo_blue.Split(',');
                    for (int i = 0; i < 255; i++)
                    {
                        db_red[i] = Convert.ToInt32(value_red[i]);
                        db_green[i] = Convert.ToInt32(value_green[i]);
                        db_blue[i] = Convert.ToInt32(value_blue[i]);
                    }

                    for (int i = 0; i < 255; i++)
                    {
                        d_red += Convert.ToInt64(Math.Pow(Math.Abs(redValues[i] - db_red[i]), 2.00));
                        d_green += Convert.ToInt64(Math.Pow(Math.Abs(greenValues[i] - db_green[i]), 2.00));
                        d_blue += Convert.ToInt64(Math.Pow(Math.Abs(blueValues[i] - db_blue[i]), 2.00));
                        /*
                        d_red += Math.Abs(redValues[i] - db_red[i]);
                        d_green += Math.Abs(greenValues[i] - db_green[i]);
                        d_blue += Math.Abs(blueValues[i] - db_blue[i]);
                        */

                    }

                    EucDis = Math.Sqrt(d_red + d_green + d_blue);
                    
                    //-----------------------------------------------------------
                    if (myVdo.Count < 15)
                    {
                        if (EucDis == 0)
                        {
                            //vdo vdo1 = new vdo(Convert.ToInt32(sql_read["index_file"].ToString()), EucDis);
                            vdo vdo1 = new vdo(Convert.ToDouble(sql_read["index_file"].ToString()), EucDis);
                            myVdo.Add(vdo1);
                        }
                        else
                        {
                            //vdo vdo1 = new vdo(Convert.ToInt32(sql_read["index_file"].ToString()), EucDis);
                            vdo vdo1 = new vdo(Convert.ToDouble(sql_read["index_file"].ToString()), EucDis);
                            myVdo.Add(vdo1);
                        }
                        
                    }
                    else
                    {
                        var min = (from item in myVdo select item.EuclDist).Min();
                        var index_min = myVdo.FindIndex(x => x.EuclDist == min);
                        var max = (from item in myVdo select item.EuclDist).Max();
                        var index_max = myVdo.FindIndex(x => x.EuclDist == max);
                        //myVdo.Where(w => (w.index = i) < min);

                        //----- rut
                        /*
                        double maxV = ab.Max();
                        if (summ < maxV)
                        {
                            int maxIndex = ab.ToList().IndexOf(maxV);
                            ab[maxIndex] = summ;
                            ab1[maxIndex] = readerx["Pathimage"].ToString();
                            ab2[maxIndex] = readerx["Pathvideo"].ToString();
                        }
                         * */

                        if (EucDis < myVdo[index_max].EuclDist)
                        {
                            vdo replace_max = new vdo(Convert.ToInt32(sql_read["index_file"].ToString()), EucDis);
                            myVdo.RemoveAt(index_max);
                            myVdo.Insert(index_max, replace_max);
                        }

                        /*
                        if(myVdo[index_min].EuclDist > EucDis)
                        {
                            vdo replace_max = new vdo(myVdo[index_min].index, min);
                            //vdo replace_min = new vdo( Convert.ToInt32(sql_read["index_file"]), EucDis);
                            vdo replace_min = new vdo(Convert.ToDouble(sql_read["index_file"]), EucDis);
                            myVdo.RemoveAt(index_min);
                            myVdo.Insert(index_min, replace_min);

                            myVdo.RemoveAt(index_max);
                            myVdo.Insert(index_max, replace_max);
                        }
                         */
                    }
                    //-----------------------------------------------------------
                }
            }
            
            con.Close();

            myVdo = myVdo.OrderBy(o => o.EuclDist).ToList();
            textBox1.Text = "";
            string kf_path = "";

            for (int i = 0; i < 15; i++)
            {
                con.Open();
                string strQuery2 = "SELECT * FROM video WHERE index_file = " + myVdo[i].index;
                SqlCommand sel_cmd2 = new SqlCommand(strQuery2, con);
                SqlDataReader sql_read2 = sel_cmd2.ExecuteReader();
                if (sql_read2.HasRows)
                {
                    while (sql_read2.Read())
                    {
                        if (i == 0)
                        {
                            kf_path = sql_read2["vdo_keyframe"].ToString();
                            pictureBox1.ImageLocation = kf_path;
                        }
                        else if (i == 1)
                        {
                            kf_path = sql_read2["vdo_keyframe"].ToString();
                            pictureBox2.ImageLocation = kf_path;
                        }
                        else if (i == 2)
                        {
                            kf_path = sql_read2["vdo_keyframe"].ToString();
                            pictureBox3.ImageLocation = kf_path;
                        }
                        else if (i == 3)
                        {
                            kf_path = sql_read2["vdo_keyframe"].ToString();
                            pictureBox4.ImageLocation = kf_path;
                        }
                        else if (i == 4)
                        {
                            kf_path = sql_read2["vdo_keyframe"].ToString();
                            pictureBox5.ImageLocation = kf_path;
                        }
                        else if (i == 5)
                        {
                            kf_path = sql_read2["vdo_keyframe"].ToString();
                            pictureBox6.ImageLocation = kf_path;
                        }
                        else if (i == 6)
                        {
                            kf_path = sql_read2["vdo_keyframe"].ToString();
                            pictureBox7.ImageLocation = kf_path;
                        }
                        else if (i == 7)
                        {
                            kf_path = sql_read2["vdo_keyframe"].ToString();
                            pictureBox8.ImageLocation = kf_path;
                        }
                        else if (i == 8)
                        {
                            kf_path = sql_read2["vdo_keyframe"].ToString();
                            pictureBox9.ImageLocation = kf_path;
                        }
                        else if (i == 9)
                        {
                            kf_path = sql_read2["vdo_keyframe"].ToString();
                            pictureBox10.ImageLocation = kf_path;
                        }
                        else if (i == 10)
                        {
                            kf_path = sql_read2["vdo_keyframe"].ToString();
                            pictureBox11.ImageLocation = kf_path;
                        }
                        else if (i == 11)
                        {
                            kf_path = sql_read2["vdo_keyframe"].ToString();
                            pictureBox12.ImageLocation = kf_path;
                        }
                        else if (i == 12)
                        {
                            kf_path = sql_read2["vdo_keyframe"].ToString();
                            pictureBox13.ImageLocation = kf_path;
                        }
                        else if (i == 13)
                        {
                            kf_path = sql_read2["vdo_keyframe"].ToString();
                            pictureBox14.ImageLocation = kf_path;
                        }
                        else if (i == 14)
                        {
                            kf_path = sql_read2["vdo_keyframe"].ToString();
                            pictureBox15.ImageLocation = kf_path;
                        }   
                        textBox1.Text += myVdo[i].index + " " + sql_read2["index_file"] + " : " + myVdo[i].EuclDist + "\r\n";
                    }
                }
                
                con.Close();
            }

            return "False";
        }

        private void ThresholdBox_TextChanged(object sender, EventArgs e)
        {
            if(ThresholdBox.Text != "" && segNameBox.Text != "")
            {
                segmentBtn.Enabled = true;
            }
            else
            {
                segmentBtn.Enabled = false;
            }
        }

        private void segNameBox_TextChanged(object sender, EventArgs e)
        {
            if (ThresholdBox.Text != "" && segNameBox.Text != "")
            {
                segmentBtn.Enabled = true;
            }
            else
            {
                segmentBtn.Enabled = false;
            }
        }

        private void histogramBtn_Click(object sender, EventArgs e)
        {
            VideoFileReader reader2 = new VideoFileReader();
            reader2.Open(path);

            int[] redValues = new int[256], redValues2 = new int[256];
            int[] greenValues = new int[256], greenValues2 = new int[256];
            int[] blueValues = new int[256], blueValues2 = new int[256];

            int sum_red = 0, sum_blue = 0, sum_green = 0;

            int[] distance = new int[Convert.ToInt32(reader2.FrameCount)];
            Histrogram h = new Histrogram();
            h.chart1.Series.Clear();
            Series series = h.chart1.Series.Add("Histrogram");

            process p = new process();

            p.progressBar1.Minimum = 0;
            p.Show();

            if (Convert.ToInt32(reader2.FrameCount) > 10000)
            {
                p.progressBar1.Maximum = 9999;
                for (int i = 0; i < 10000; i++)             // read video frames out of it
                {
                    sum_red = 0; sum_green = 0; sum_blue = 0;

                    using (Bitmap videoFrame = reader2.ReadVideoFrame())
                    {
                        if (i != 0)
                        {
                            // RGB
                            ImageStatistics rgbStatistics2 = new ImageStatistics(videoFrame);
                            redValues2 = rgbStatistics2.Red.Values;
                            greenValues2 = rgbStatistics2.Green.Values;
                            blueValues2 = rgbStatistics2.Blue.Values;

                            for (int j = 0; j < 255; j++)
                            {
                                sum_red = sum_red + Math.Abs(redValues[j] - redValues2[j]);
                                sum_green = sum_green + Math.Abs(greenValues[j] - greenValues2[j]);
                                sum_blue = sum_blue + Math.Abs(blueValues[j] - blueValues2[j]);
                            }
                            distance[i] = Math.Abs(sum_red + sum_green + sum_blue);
                            series.Points.Add(distance[i]);
                            p.progressBar1.Value = i;
                            int percent = (int)(((double)p.progressBar1.Value / (double)p.progressBar1.Maximum) * 100);
                            p.progressBar1.CreateGraphics().DrawString(percent.ToString() + "%", new Font("Arial", (float)8.25, FontStyle.Regular), Brushes.Black, new PointF(p.progressBar1.Width / 2 - 10, p.progressBar1.Height / 2 - 7));

                        }

                        //if (i != Convert.ToInt32(reader.FrameCount) - 1)
                        //{
                        // RGB
                        ImageStatistics rgbStatistics = new ImageStatistics(videoFrame);
                        redValues = rgbStatistics.Red.Values;
                        greenValues = rgbStatistics.Green.Values;
                        blueValues = rgbStatistics.Blue.Values;

                        //}
                        videoFrame.Dispose();
                    }
                }
                reader2.Close();
                h.Show();
                p.Close();
            }
            else
            {
                p.progressBar1.Maximum = Convert.ToInt32(reader2.FrameCount) - 1;
                for (int i = 0; i < Convert.ToInt32(reader2.FrameCount); i++)             // read video frames out of it
                {
                    sum_red = 0; sum_green = 0; sum_blue = 0;

                    using (Bitmap videoFrame = reader2.ReadVideoFrame())
                    {
                        if (i != 0)
                        {
                            // RGB
                            ImageStatistics rgbStatistics2 = new ImageStatistics(videoFrame);
                            redValues2 = rgbStatistics2.Red.Values;
                            greenValues2 = rgbStatistics2.Green.Values;
                            blueValues2 = rgbStatistics2.Blue.Values;

                            for (int j = 0; j < 255; j++)
                            {
                                sum_red = sum_red + Math.Abs(redValues[j] - redValues2[j]);
                                sum_green = sum_green + Math.Abs(greenValues[j] - greenValues2[j]);
                                sum_blue = sum_blue + Math.Abs(blueValues[j] - blueValues2[j]);
                            }
                            distance[i] = Math.Abs(sum_red + sum_green + sum_blue);
                            series.Points.Add(distance[i]);
                            p.progressBar1.Value = i;
                            int percent = (int)(((double)p.progressBar1.Value / (double)p.progressBar1.Maximum) * 100);
                            p.progressBar1.CreateGraphics().DrawString(percent.ToString() + "%", new Font("Arial", (float)8.25, FontStyle.Regular), Brushes.Black, new PointF(p.progressBar1.Width / 2 - 10, p.progressBar1.Height / 2 - 7));

                        }

                        //if (i != Convert.ToInt32(reader.FrameCount) - 1)
                        //{
                        // RGB
                        ImageStatistics rgbStatistics = new ImageStatistics(videoFrame);
                        redValues = rgbStatistics.Red.Values;
                        greenValues = rgbStatistics.Green.Values;
                        blueValues = rgbStatistics.Blue.Values;

                        //}
                        videoFrame.Dispose();
                    }
                }
                reader2.Close();
                h.Show();
                p.Close();
            }

        }

        private void searchKfPb_Click(object sender, EventArgs e)
        {
            if (searchKfPb.Image != null)
            {
                player pv = new player(path);
                pv.ShowDialog();
            }
        }
        
        private string getVideo(string KF_path)
        {
            string r = "";
            textBox1.Text = KF_path;
            
            string constr = ConfigurationManager.ConnectionStrings["Dbconnection"].ConnectionString;
            SqlConnection con = new SqlConnection(constr);
            con.Open();

            string QueryStr = "select vdo_shot from video where vdo_keyframe = '"+ KF_path +"' ";
            SqlCommand selCmd = new SqlCommand(QueryStr, con);
            SqlDataReader sqlRead = selCmd.ExecuteReader();
            if (sqlRead.HasRows)
            {
                while (sqlRead.Read())
                {
                    r = sqlRead["vdo_shot"].ToString();
                }
            }

            con.Close();
            
            return r;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            var pb = sender as PictureBox;
            string pbb = "";
            string pvdo = "";
            if (pb != null && pb.ImageLocation != null)
            {
                pbb = pb.ImageLocation;

                pvdo = getVideo(pbb);

                //textBox1.Text = pvdo.ToString();       

                player pv = new player(pvdo);
                pv.ShowDialog();
            }


        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

    }
}

