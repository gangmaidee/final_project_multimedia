﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AForge.Video;
using AForge.Video.FFMPEG;
using AForge.Imaging;
using System.Drawing;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;

namespace FinalProjectMulti
{
    class VideoSearch
    {
        public struct vdo
        {
            private int _index;
            private int _euclDist;

            public int index
            {
                get { return _index; }
                set { _index = value; }
            }

            public int EuclDist
            {
                get { return _euclDist; }
                set { _euclDist = value; }
            }

            public vdo(int Index, int EuclDist)
            {
                this._index = Index;
                this._euclDist = EuclDist;
            }

        };

        public static string VdoSearch(string path)
        {
            int[] redValues = new int[256], db_red = new int[256];
            int[] greenValues = new int[256], db_green = new int[256];
            int[] blueValues = new int[256], db_blue = new int[256];

            int d_red = 0, d_green = 0, d_blue = 0;      
            int EucDis = 0;
            List<vdo> myVdo = new List<vdo>();

            string name = "", vdo_shot = "", vdo_keyframe = "", vdo_red = "", vdo_green = "", vdo_blue = "";
            
            VideoFileReader reader = new VideoFileReader();
            reader.Open(path);

            int keyFrame = Convert.ToInt32(reader.FrameCount);

            if (keyFrame % 2 == 0)
            { keyFrame = keyFrame / 2; }
            else
            { keyFrame = (keyFrame + 1) / 2; }

            for (int z = 0; z < Convert.ToInt32(reader.FrameCount); z++)
            {
                Bitmap videoFrame = reader.ReadVideoFrame();
                if (z == keyFrame)
                {
                    ImageStatistics rgbStatistics = new ImageStatistics(videoFrame);
                    redValues = rgbStatistics.Red.Values;
                    greenValues = rgbStatistics.Green.Values;
                    blueValues = rgbStatistics.Blue.Values;
                    break;
                }
                videoFrame.Dispose();
                videoFrame = null;
            }

            reader.Close();

            string constr = ConfigurationManager.ConnectionStrings["Dbconnection"].ConnectionString;
            SqlConnection con = new SqlConnection(constr);
            con.Open();

            string strQuery = "SELECT * FROM video ORDER BY index_file";

            SqlCommand sel_cmd = new SqlCommand(strQuery, con);
            SqlDataReader sql_read = sel_cmd.ExecuteReader();
            if (sql_read.HasRows)
            {
                while (sql_read.Read())
                {
                    //-----------------------------------------------------------
                    vdo_red = sql_read["vdo_red"].ToString();
                    vdo_green = sql_read["vdo_green"].ToString();
                    vdo_blue = sql_read["vdo_blue"].ToString();
                    string[] value_red = vdo_red.Split(','), 
                        value_green = vdo_green.Split(','),
                        value_blue = vdo_blue.Split(',');
                    for(int i = 0; i < 255; i++)
                    {
                        db_red[i] = Convert.ToInt32(value_red[i]);
                        db_green[i] = Convert.ToInt32(value_green[i]);
                        db_blue[i] = Convert.ToInt32(value_blue[i]);
                    }

                    for (int i = 0; i < 255; i++)
                    {                                          

                        d_red += Math.Abs(redValues[i] - db_red[i]);
                        d_green += Math.Abs(greenValues[i] - db_green[i]);
                        d_blue += Math.Abs(blueValues[i] - db_blue[i]);
                                          
                    }

                    EucDis = d_red + d_green + d_blue;
                    //-----------------------------------------------------------
                    if (myVdo.Count < 20)
                    {
                        vdo vdo1 = new vdo(Convert.ToInt32(sql_read["index_file"].ToString()), EucDis);
                        myVdo.Add(vdo1);
                    }
                    else
                    {
                        var min = (from item in myVdo select item.EuclDist).Min();
                        var index_min = myVdo.FindIndex(x => x.EuclDist == min );
                        //myVdo.Where(w => (w.index = i) < min);

                        if(EucDis < myVdo[index_min].EuclDist)
                        {
                            vdo vdo2 = new vdo(Convert.ToInt32(sql_read["index_file"].ToString()), EucDis);
                            myVdo.RemoveAt(index_min);
                            myVdo.Insert(index_min, vdo2);
                        }
                    }
                    //-----------------------------------------------------------
                }
            }
            myVdo.OrderBy( o => o.EuclDist ).ToList();

            Form1 frm1 = new Form1();            

            con.Close();
            int countVideo = 0;
            for (int i = 0; i < 5; i++)
            {
                con.Open();
                string strQuery2 = "SELECT * FROM video WHERE index_file = " + myVdo[i].index;
                SqlCommand sel_cmd2 = new SqlCommand(strQuery2, con);
                SqlDataReader sql_read2 = sel_cmd2.ExecuteReader();
                if (sql_read2.HasRows)
                {
                    while (sql_read2.Read())
                    {
                        if (i == 1)
                        {
                            string kf_path = sql_read2["vdo_keyframe"].ToString();
                            frm1.pictureBox1.Load(kf_path);
                            return kf_path;
                        }
                        else if(i == 2)
                        {
                            string kf_path = sql_read2["vdo_keyframe"].ToString();
                            frm1.pictureBox2.ImageLocation = kf_path;
                        }
                        else if (i == 3)
                        {
                            string kf_path = sql_read2["vdo_keyframe"].ToString();
                            frm1.pictureBox3.ImageLocation = kf_path;
                        }
                        else if (i == 4)
                        {
                            string kf_path = sql_read2["vdo_keyframe"].ToString();
                            frm1.pictureBox4.ImageLocation = kf_path;
                        }
                        else if (i == 5)
                        {
                            string kf_path = sql_read2["vdo_keyframe"].ToString();
                            frm1.pictureBox5.ImageLocation = kf_path;
                        }
                    }
                }
                con.Close();
            }

            return "555";
        }
    }
}
