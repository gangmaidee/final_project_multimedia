﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

using System.Diagnostics;
using AForge;
using AForge.Video;
using AForge.Video.FFMPEG;
using AForge.Math;
using AForge.Imaging;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;

namespace FinalProjectMulti
{
    
    class VideoSegmentation
    {
        static int countKeyframe = 1;

        public static void VideoSegment(string path, string sText, string savePath, int Thresholds)
        {
            Form1 frm1 = new Form1();

            int[] redValues = new int[256], redValues2 = new int[256];
            int[] greenValues = new int[256], greenValues2 = new int[256];
            int[] blueValues = new int[256], blueValues2 = new int[256];

            int sum_red = 0, sum_blue = 0, sum_green = 0;
            int Threshold = Thresholds, countShot = 1, FrameCount = 1;
            string name = "", vdo_path = "", keyframe_path = "", vdo_red = "", vdo_green = "", vdo_blue = "";

            Bitmap KeyFrame;
            int[] KF_redValues = new int[256];
            int[] KF_greenValues = new int[256];
            int[] KF_blueValues = new int[256];
            ImageStatistics KF_rgbStatistics;
            
            VideoFileReader reader = new VideoFileReader();
            reader.Open(path);

            int width = reader.Width; int height = reader.Height;
            int frameRate = reader.FrameRate;
            VideoFileWriter writer = new VideoFileWriter();             // create instance of video writer
            name = sText+"_part0.avi";
            Directory.CreateDirectory(@savePath + @"\" + sText + "_VDO");
            Directory.CreateDirectory(@savePath + @"\" + sText + "_PIC");
            string sPath = @savePath + @"\" + sText + @"_VDO";
            string sPath_pic = @savePath + @"\" + sText + @"_PIC";
            vdo_path = @sPath + @"\" + sText + "_part1.avi"; ; // @"C:\Users\TNP\Videos\Test\" + sText + "_part1.avi";
            writer.Open(vdo_path, width, height, frameRate, VideoCodec.MPEG4);             // create new video file

            int[] distance = new int[Convert.ToInt32(reader.FrameCount)];

            process p = new process();
            p.progressBar1.Minimum = 0;
            p.progressBar1.Maximum = Convert.ToInt32(reader.FrameCount) - 1;
            p.Show();

            for (int i = 0; i < Convert.ToInt32(reader.FrameCount); i++)             // read video frames out of it
            {
                sum_red = 0; sum_green = 0; sum_blue = 0;

                using (Bitmap videoFrame = reader.ReadVideoFrame())
                {
                    if (i != 0)
                    {
                        // RGB
                        ImageStatistics rgbStatistics2 = new ImageStatistics(videoFrame);
                        redValues2 = rgbStatistics2.Red.Values;
                        greenValues2 = rgbStatistics2.Green.Values;
                        blueValues2 = rgbStatistics2.Blue.Values;

                        for (int j = 0; j < 255; j++)
                        {
                            sum_red = sum_red + Math.Abs(redValues[j] - redValues2[j]);
                            sum_green = sum_green + Math.Abs(greenValues[j] - greenValues2[j]);
                            sum_blue = sum_blue + Math.Abs(blueValues[j] - blueValues2[j]);
                        }
                        distance[i] = Math.Abs(sum_red + sum_green + sum_blue);
                    }

                    //if (i != Convert.ToInt32(reader.FrameCount) - 1)
                    //{
                        // RGB
                        ImageStatistics rgbStatistics = new ImageStatistics(videoFrame);
                        redValues = rgbStatistics.Red.Values;
                        greenValues = rgbStatistics.Green.Values;
                        blueValues = rgbStatistics.Blue.Values;

                    //}

                    FrameCount++;
                    if (distance[i] > Threshold && FrameCount > reader.FrameRate-1)
                    {
                        countShot++;
                        writer.Close();
                        keyframe_path = search_keyframe(vdo_path, sText, sPath_pic);

                        KeyFrame = new Bitmap(@keyframe_path);
                        KF_rgbStatistics = new ImageStatistics(KeyFrame);
                        KF_redValues = KF_rgbStatistics.Red.Values;
                        KF_greenValues = KF_rgbStatistics.Green.Values;
                        KF_blueValues = KF_rgbStatistics.Blue.Values;

                        vdo_red = string.Join(",", KF_redValues);
                        vdo_green = string.Join(",", KF_greenValues);
                        vdo_blue = string.Join(",", KF_blueValues);

                        string constr = ConfigurationManager.ConnectionStrings["Dbconnection"].ConnectionString;
                        SqlConnection con = new SqlConnection(constr);
                        con.Open();

                        string strQuery = "INSERT into video VALUES (@name, @vdo_shot, @vdo_keyframe, @vdo_red, @vdo_green, @vdo_blue)";

                        SqlCommand ins_cmd = new SqlCommand(strQuery, con);
                        ins_cmd.Parameters.Add(new SqlParameter("name", name));
                        ins_cmd.Parameters.Add(new SqlParameter("vdo_shot", vdo_path));
                        ins_cmd.Parameters.Add(new SqlParameter("vdo_keyframe", keyframe_path));
                        ins_cmd.Parameters.Add(new SqlParameter("vdo_red", vdo_red));
                        ins_cmd.Parameters.Add(new SqlParameter("vdo_green", vdo_green));
                        ins_cmd.Parameters.Add(new SqlParameter("vdo_blue", vdo_blue));
                        ins_cmd.ExecuteNonQuery();

                        con.Close();

                        FrameCount = 0;
                        name = sText + "_part" + countShot.ToString() + ".avi";
                        //vdo_path = @"C:\Users\TNP\Videos\Test\" + sText + "_part" + countShot.ToString() + ".avi";
                        vdo_path = @sPath + @"\" + sText + "_part" + countShot.ToString() + ".avi";
                        writer.Open(vdo_path, width, height, reader.FrameRate, VideoCodec.MPEG4);
                        writer.WriteVideoFrame(videoFrame);
                    }
                    else
                    {
                        writer.WriteVideoFrame(videoFrame);
                    }
                    videoFrame.Dispose();
                }
                p.progressBar1.Value = i;
                int percent = (int)(((double)p.progressBar1.Value / (double)p.progressBar1.Maximum) * 100);
                p.progressBar1.CreateGraphics().DrawString(percent.ToString() + "%", new Font("Arial", (float)8.25, FontStyle.Regular), Brushes.Black, new PointF(p.progressBar1.Width / 2 - 10, p.progressBar1.Height / 2 - 7));
            }
            reader.Close();
            writer.Close();
            p.Close();
            keyframe_path = search_keyframe(vdo_path, sText, sPath_pic);
            
            KeyFrame = new Bitmap(@keyframe_path);
            KF_rgbStatistics = new ImageStatistics(KeyFrame);
            KF_redValues = KF_rgbStatistics.Red.Values;
            KF_greenValues = KF_rgbStatistics.Green.Values;
            KF_blueValues = KF_rgbStatistics.Blue.Values;
            
            vdo_red = string.Join(",", KF_redValues);
            vdo_green = string.Join(",", KF_greenValues);
            vdo_blue = string.Join(",", KF_blueValues);

            string constr2 = ConfigurationManager.ConnectionStrings["Dbconnection"].ConnectionString;
            SqlConnection con2 = new SqlConnection(constr2);
            con2.Open();

            string strQuery2 = "INSERT into video VALUES (@name, @vdo_shot, @vdo_keyframe, @vdo_red, @vdo_green, @vdo_blue)";

            SqlCommand ins_cmd2 = new SqlCommand(strQuery2, con2);
            ins_cmd2.Parameters.Add(new SqlParameter("name", name));
            ins_cmd2.Parameters.Add(new SqlParameter("vdo_shot", vdo_path));
            ins_cmd2.Parameters.Add(new SqlParameter("vdo_keyframe", keyframe_path));
            ins_cmd2.Parameters.Add(new SqlParameter("vdo_red", vdo_red));
            ins_cmd2.Parameters.Add(new SqlParameter("vdo_green", vdo_green));
            ins_cmd2.Parameters.Add(new SqlParameter("vdo_blue", vdo_blue));
            ins_cmd2.ExecuteNonQuery();

            con2.Close();

        }

        public static string search_keyframe(string name, string sText, string savePathPic)
        {            
            VideoFileReader reader2 = new VideoFileReader();
            reader2.Open(name);

            string keyframe_path = "";

            int keyFrame = Convert.ToInt32(reader2.FrameCount);

            if (keyFrame % 2 == 0)
            { keyFrame = keyFrame / 2; }
            else
            { keyFrame = (keyFrame + 1) / 2; }

            for (int z = 0; z < Convert.ToInt32(reader2.FrameCount); z++)
            {
                Bitmap videoFrame2 = reader2.ReadVideoFrame();
                if (z == keyFrame)
                {
                    //keyframe_path = @"C:\Users\TNP\Pictures\Test\KF_" + sText + "_part" + countKeyframe + ".jpg";
                    keyframe_path = @savePathPic + @"\KF_" + sText + "_part" + countKeyframe + ".jpg";
                    videoFrame2.Save(keyframe_path, ImageFormat.Jpeg);
                    countKeyframe++;
                    break;
                }
                videoFrame2.Dispose();
                videoFrame2 = null;
            }
            
            reader2.Close();

            return keyframe_path;
        }

    }
}
